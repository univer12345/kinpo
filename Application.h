#ifndef APPLICATION_H
#define APPLICATION_H
#include "QString"
#include <QDate>


enum indexGender {male, female};    //Пол
enum subjects {math1, math2, phis1, phis2, rus};    //Предмет
enum languages {english, japan, franch, germany, korean, chaina};   //Язык
enum indexExams {math01, math02, phis01, phis02, rus0}; //Экзамен

class Application
{
protected:
    int id;
    QString name;   //ФИО
    QDate dateBirth;  //Дата рождения
    indexGender gender;    //Пол
    indexExams currentExam;
    subjects subject[3];    //Предметы
    languages language;   //Язык
    bool haveAMedal;    //Наличие медали
    double averageValue;    //Средняя оценка
    int raitingOne; //Баллы по первому предмету
    int raitingTwo; //Баллы по второму предмету
    int raitingThree;   //Баллу по третьему предмету
    int payStudy;   //Согласие на платную учебу
    int exam;
public:
    double averageScoreOfSub(int, int, int);    //Средний балл по предметам
    bool operator>(const Application &right);   //Перегрузка оператора больше
    bool operator==(const Application &right);  //Перегрузка оператора равно
    void createDefaultRec();    //Создание стандартной записи

    Application(QString name, QDate dateBirth, indexGender gender, indexExams currentExam,
    languages language, bool haveAMedal, double averageValue, int raitingOne, int raitingTwo, int raitingThree, int payStudy, int exam);    //Конструктор
    QString getName() const;    //Получить имя
    void setName(const QString &value); //Установить имя
    QDate getDateBirth() const; //Получить дату рождения
    void setDateBirth(const QDate &value);  //Установить дату рождения
    int getIndexGender() const; //Получит пол
    void setIndexGender(int value); //Установить пол
    int getIndexExam() const;   //Полчить индекс экзамена
    void setIndexExam(int value);   //Установить индекс экзамена
    bool getHaveAMedal() const; //Получить инфо о наличии медали
    void setHaveAMedal(bool value); //Установить/снять флажок о наличии медали
    double getAverageValue() const; //Получить среднее значение аттестата
    void setAverageValue(double value); //Установить среднее значение аттестата

    //Получить/Установить баллы по предметам
    int getRaitingOne() const;
    void setRaitingOne(int value);
    int getRaitingTwo() const;
    void setRaitingTwo(int value);
    int getRaitingThree() const;
    void setRaitingThree(int value);

    int getLanguage() const;    //Получить язык
    void setLanguage(int value);    //Установить язык
    int getSubject (int index) const;   //Получить предметы
    void setSubject (int index, int value); //Усьановить предметы
    int getPayStudy() const;    //Получит инфо о желании учиться платно
    void setPayStudy(int value);    //Установить/снять флажок об платной учебе
    int getExam() const;    //Получить инфо о наличии экзамена
    void setExam(int value);    //Установить инфо о наличии экзамена

    Application();   //Конструктор класса

    int getId() const;  //Получи айди
    void setId(int value);  //Установиь айди
    QString recordInStr(int);
    Application fromServer(QString);
};

#endif // APPLICATION_H
