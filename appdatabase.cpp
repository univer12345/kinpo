#include "appdatabase.h"

appDataBase::appDataBase()  //Конструктор
{
    this->lastId = 0;
}

int appDataBase::createApp(Application &app)    //Добавление элемента в массив записей
{
    this->lastId++;
    app.setId(this->lastId);
    this->save.append(app);
    return this->lastId;
}

Application &appDataBase::getbyId(int id) //Получение записи по id
{
    for (int i = 0; i < this->save.size(); i++)
    {
        if (id == this->save.at(i).getId())
        {
            return this->save[i];
        }
    }
    throw id;
}

bool appDataBase::deletebyId(int id)    //Удаление записи из массива записей
{
    for (int i = 0; i < this->save.size(); i++)
    {
        if (id == this->save.at(i).getId())
        {
            this->save.remove(i);
            return true;
        }
    }
    return false;
}
