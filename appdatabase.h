#ifndef APPDATABASE_H
#define APPDATABASE_H
#include "Application.h"
#include <QVector>

class appDataBase
{
private:
    int lastId; //Количество записей всего(удаленные тоже считаются)
    QVector<Application> save;  //Контейнер записей


public:
    appDataBase();
    int createApp(Application &app);    //Создать запись в базе данных
    Application& getbyId(int id);   //Получить айдишник записи из базы данных
    bool deletebyId(int id);    //Удалить запись в базе данных
};

#endif // APPDATABASE_H
