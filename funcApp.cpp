#include "Application.h"
#include <math.h>
#include <QString>

Application::Application()   //Конструктор класса
{
    this->setId(-1);
    this->name = "ФИО";
    this->dateBirth = QDate(2000,1,1);
    this->gender = male;
    this->currentExam = math01;
    this->haveAMedal = false;
    this->averageValue = 3.0;
    this->language = english;
    this->subject[0] = {math1};
    this->subject[1] = {math2};
    this->subject[2] = {phis1};
    this->raitingOne = 0;
    this->raitingThree = 0;
    this->raitingTwo = 0;
    this->payStudy = false;
    this->exam = false;
}

int Application::getId() const
{
    return id;
}

void Application::setId(int value)
{
    id = value;
}
QString Application::getName() const
{
    return name;
}
void Application::setName(const QString &value)
{
    name = value;
}
QDate Application::getDateBirth() const
{
    return dateBirth;
}
void Application::setDateBirth(const QDate &value)
{
    dateBirth = value;
}
int Application::getIndexGender() const
{
    return gender;
}
void Application::setIndexGender(int value)
{
    if (value == 0) gender = male;
    else if (value == 1) gender = female;
}
int Application::getIndexExam() const
{
    return currentExam;
}
void Application::setIndexExam(int value)
{
    if (value == 0) currentExam = math01;
    if (value == 1) currentExam = math02;
    if (value == 2) currentExam = phis01;
    if (value == 3) currentExam = phis02;
    if (value == 4) currentExam = rus0;
}
bool Application::getHaveAMedal() const
{
    return haveAMedal;
}
void Application::setHaveAMedal(bool value)
{
    haveAMedal = value;
}
double Application::getAverageValue() const
{
    return averageValue;
}
void Application::setAverageValue(double value)
{
    averageValue = value;
}
int Application::getRaitingOne() const
{
    return raitingOne;
}
void Application::setRaitingOne(int value)
{
    raitingOne = value;
}
int Application::getRaitingTwo() const
{
    return raitingTwo;
}
void Application::setRaitingTwo(int value)
{
    raitingTwo = value;
}
int Application::getRaitingThree() const
{
    return raitingThree;
}
void Application::setRaitingThree(int value)
{
    raitingThree = value;
}
int Application::getLanguage() const
{
    return language;
}
void Application::setLanguage(int value)
{
    if (value == 0){language = english;}
    if (value == 1){language = japan;}
    if (value == 2){language = franch;}
    if (value == 3){language = germany;}
    if (value == 4){language = korean;}
    if (value == 5){language = chaina;}
}
int Application::getSubject (int index) const
{
    return subject[index];
}
void Application::setSubject (int index, int value)
{
    if (value == 0){subject[index] = math1;}
    if (value == 1){subject[index] = math2;}
    if (value == 2){subject[index] = phis1;}
    if (value == 3){subject[index] = phis2;}
    if (value == 4){subject[index] = rus;}
}
int Application::getPayStudy() const
{
    return payStudy;
}
void Application::setPayStudy(int value)
{
    payStudy = value;
}

int Application::getExam() const
{
    return exam;
}
void Application::setExam(int value)
{
    exam = value;
}

//Перегрузка оператора больше
bool Application::operator>(const Application &right)
{
   bool answer = false;
   QChar qlef = this->name[0];  //Первая буква левого
   QChar qrig = right.name[0];  //Первая буква правого
   double lef = this->averageScoreOfSub(this->raitingOne, this->raitingTwo, this->raitingThree);    //Средний балл левого
   double rig = this->averageScoreOfSub(right.raitingOne, right.raitingTwo, right.raitingThree);    //Средний балл правого
   if (this->haveAMedal && !right.haveAMedal)   //Если медаль только у левого
   {
       answer = true;
   }
   else if ((this->haveAMedal && right.haveAMedal) || (!this->haveAMedal && !right.haveAMedal))    //Если равная ситуация
   {
       if (lef > rig)  //Если средний балл больше у первого
       {
           answer = true;
       }
       else if (lef == rig)  //Если средний балл одинаковый
       {
           if (QString::compare(qlef, qrig,Qt::CaseInsensitive) < 0) //Если ФИО правого больше левого
           {
               answer = true;
           }
       }
   }
   if (this->name == "ФИО") //Если поле ФИО == "ФИО"
   {
       answer = false;
   }
   return answer;
}

//Перегрузка оператора равно
bool Application::operator==(const Application &right)
{
    bool answer = false;
    QChar qlef = this->name[0];  //Первая буква левого
    QChar qrig = right.name[0];  //Первая буква правого
    double lef = this->averageScoreOfSub(this->raitingOne, this->raitingTwo, this->raitingThree);    //Средний балл левого
    double rig = this->averageScoreOfSub(right.raitingOne, right.raitingTwo, right.raitingThree);    //Средний балл правого
    if (this->haveAMedal && right.haveAMedal)   //Если у обоих есть медали
    {
        if (lef == rig) //Если средние баллы равны
        {
            if (QString::compare(qlef, qrig,Qt::CaseInsensitive) == 0)   //Если ФИО равны
            {
                answer = true;
            }
        }
    }
    return answer;
}

//Начальные данные в записи при создании
void Application::createDefaultRec()
{
    this->setId(-1);
    this->name = "ФИО";
    this->dateBirth = QDate(2000,1,1);
    this->gender = male;
    this->currentExam = math01;
    this->haveAMedal = false;
    this->averageValue = 3.0;
    this->language = english;
    this->subject[0] = {math1};
    this->subject[1] = {math2};
    this->subject[2] = {phis1};
    this->raitingOne = 0;
    this->raitingThree = 0;
    this->raitingTwo = 0;
    this->payStudy = false;
    this->exam = false;
}

//Конструктор класса
Application::Application(QString name, QDate dateBirth, indexGender gender, indexExams currentExam,
languages language, bool haveAMedal, double averageValue, int raitingOne, int raitingTwo, int raitingThree, int payStudy, int exam)
{
    this->name = name;
    this->dateBirth = dateBirth;
    this->gender = gender;
    this->currentExam = currentExam;
    this->haveAMedal = haveAMedal;
    this->averageValue = averageValue;
    this->language = language;
    this->subject[0] = {math1};
    this->subject[1] = {math2};
    this->subject[2] = {phis1};
    this->raitingOne = raitingOne;
    this->raitingThree = raitingThree;
    this->raitingTwo = raitingTwo;
    this->payStudy = payStudy;
    this->exam = exam;
}

//Вычисление среднего балла
double Application::averageScoreOfSub(int arg1, int arg2, int arg3)
{
    return floor((arg1 + arg2 + arg3) * 10 / 3) / 10;
}

QString Application::recordInStr(int id)
{
    QString str;
    str += QString::number(id);
    str += "!";
    str += this->getName();
    str += "!";
    str += this->getDateBirth().toString(Qt::ISODate);
    str += "!";
    str += QString::number(this->getIndexGender());
    str += "!";
    str += QString::number(this->getHaveAMedal());
    str += "!";
    str += QString::number(this->getAverageValue(), 'f', 2);
    str += "!";
    str += QString::number(this->getLanguage());
    str += "!";
    str += QString::number(this->getSubject(0));
    str += "!";
    str += QString::number(this->getSubject(1));
    str += "!";
    str += QString::number(this->getSubject(2));
    str += "!";
    str += QString::number(this->getRaitingOne());
    str += "!";
    str += QString::number(this->getRaitingTwo());
    str += "!";
    str += QString::number(this->getRaitingThree());
    str += "!";
    str += QString::number(this->getIndexExam());
    str += "!";
    str += QString::number(this->getExam());
    str += "!";
    str += QString::number(this->getPayStudy());
    str += "!";
    return str;
}

Application Application::fromServer(QString str)
{
    QStringList copyStr = str.split('!');
    Application app;
    app.setId(copyStr[0].toInt());
    app.setName(copyStr[1]);
    app.setDateBirth(QDate::fromString(copyStr[2], "yyyy-MM-dd"));
    app.setIndexGender(copyStr[3].toInt());
    app.setHaveAMedal(copyStr[4].toInt());
    app.setAverageValue(copyStr[5].toDouble());
    app.setLanguage(copyStr[6].toInt());
    app.setSubject(0, copyStr[7].toInt());
    app.setSubject(1, copyStr[8].toInt());
    app.setSubject(2, copyStr[9].toInt());
    app.setRaitingOne(copyStr[10].toInt());
    app.setRaitingTwo(copyStr[11].toInt());
    app.setRaitingThree(copyStr[12].toInt());
    app.setIndexExam(copyStr[13].toInt());
    app.setExam(copyStr[14].toInt());
    app.setPayStudy(copyStr[15].toInt());
    return app;
}




