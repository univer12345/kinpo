#include "mainwindow.h"
#include <QMessageBox>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.setWindowTitle("Application");
    a.connect(&a, SIGNAL(focusChanged(QWidget *, QWidget *)), &w, SLOT(saveData()));    //Активирует сигнал перевода фокуса
    w.show();
    return a.exec();
}
