#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include "validator.h"
#include <QStandardItemModel >
#include<QDebug>
#include <QString>
#include <QDate>
#include <math.h>
//#include "Application.h"
#include <QVector>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    socket.connectToServer("appPipe",QIODevice::ReadWrite);
    start();    //Начальные значения
}

MainWindow::~MainWindow()
{
    delete ui;
}

//Сохранение данных по переводу фокуса
void MainWindow::saveData()
{


    //gdhrjthjfjfgjghktrkjghkfg
    /*gdfshfdf
hsdhndfhn*/
    if (ui->recBrowser->rowCount() != 0 && perehod == 0)    //Если есть записи в таблице
    {
        if (ui->FIO->text() == "ФИО")   //Если поле ФИО == "ФИО"
        {
            doEnabledNewRecord(false);  //Диактивировать все поля кроме ФИО
        }
        //socket.write(QString(QString("save") + "\n").toUtf8());
        //socket.waitForReadyRead(125);
        //socket.waitForReadyRead(125);
        //QString str = socket.readLine(10000);
        arguments.clear();
        arguments << "save" << QString::number(currentRec);
        launchServ(arguments);
        QString str = getResultFromServer();
        curApp = curApp.fromServer(str);
        curApp.setHaveAMedal(ui->presenceOfMedal->isChecked());
        curApp.setDateBirth(ui->DateOfBirth->date());    //Сохранение даты рождения
        curApp.setIndexGender(ui->gender->currentIndex());   //Сохранение пола
        curApp.setAverageValue(ui->averageRating->value());  //Сохранение среднего значения
        curApp.setLanguage(ui->chooseLanguages->currentIndex()); //Сохранение выбранного языка
        curApp.setSubject(0, ui->subjectOne->currentIndex());    //Сохранение первого предмета
        curApp.setSubject(1, ui->subjectTwo->currentIndex());    //Сохранение второго предмета
        curApp.setSubject(2, ui->subjectThree->currentIndex());  //Сохранение третьего предмета
        curApp.setPayStudy(ui->contractualStudy->isChecked());   //Сохранение инфо об готовности учиться платно
        curApp.setExam(ui->credit->isChecked());
        curApp.setIndexExam(ui->exam_sub->currentIndex());
        str = curApp.recordInStr(curApp.getId());
        arguments.clear();
        arguments << "changedById" << str << QString::number(currentRec);
        launchServ(arguments);
        //socket.write(QString(str + "\n").toUtf8());
        //socket.waitForReadyRead(125);
    }
}

//Блокировка/разблокирока предметов в зависимости от выбора первого предмета
void MainWindow::on_subjectOne_currentIndexChanged(int index)
{
      //Устанавливаем QComboBox не первый, выбираем предыдущий элемент и разблокируем его, затем, блокируем выбранный
    QStandardItemModel* model = dynamic_cast< QStandardItemModel * >( ui->subjectTwo->model() );    //Для второго предмета
    QStandardItem* item = model->item(priveousSubjectOne);
    item->setEnabled( true );
    item = model->item( index );
    item->setEnabled( false );

        model = dynamic_cast< QStandardItemModel * >( ui->subjectThree->model() );  //Для третьего предмета
        item = model->item(priveousSubjectOne);
        item->setEnabled( true );
        item = model->item( index );
        item->setEnabled( false );

        model = dynamic_cast< QStandardItemModel * >( ui->exam_sub->model() );  //Для модификации
        item = model->item(priveousSubjectOne);
        item->setEnabled( false );
        item = model->item( index );
        item->setEnabled( true );

        //Выбор экзамена в зависимости от предметов
        if (ui->exam_sub->currentIndex() != ui->subjectTwo->currentIndex() &&
                ui->exam_sub->currentIndex() != ui->subjectThree->currentIndex())
        {
            ui->exam_sub->setCurrentIndex(index);
        }
        priveousSubjectOne = index;
}

//Блокировка/разблокирока предметов в зависимости от выбора второго предмета
void MainWindow::on_subjectTwo_currentIndexChanged(int index)
{   
    //Устанавливаем QComboBox не второй, выбираем предыдущий элемент и разблокируем его, затем, блокируем выбранный
    QStandardItemModel* model = dynamic_cast< QStandardItemModel * >( ui->subjectOne->model() );    //Для первого предмета
    QStandardItem* item = model->item(priveousSubjectTwo);
    item->setEnabled( true );
    item = model->item( index );
    item->setEnabled( false );

    model = dynamic_cast< QStandardItemModel * >( ui->subjectThree->model() );  //Для третьего предмета
    item = model->item(priveousSubjectTwo);
    item->setEnabled( true );
    item = model->item( index );
    item->setEnabled( false );

    model = dynamic_cast< QStandardItemModel * >( ui->exam_sub->model() );  //Для модификации
    item = model->item(priveousSubjectTwo);
    item->setEnabled( false );
    item = model->item( index );
    item->setEnabled( true );

    //Выбор экзамена в зависимости от предметов
    if (ui->exam_sub->currentIndex() != ui->subjectOne->currentIndex() &&
            ui->exam_sub->currentIndex() != ui->subjectThree->currentIndex())
    {
        ui->exam_sub->setCurrentIndex(index);
    }
    priveousSubjectTwo = index;
}

//Блокировка/разблокирока предметов в зависимости от выбора третьего предмета
void MainWindow::on_subjectThree_currentIndexChanged(int index)
{
    //Устанавливаем QComboBox не третий, выбираем предыдущий элемент и разблокируем его, затем, блокируем выбранный
    QStandardItemModel* model = dynamic_cast< QStandardItemModel * >( ui->subjectOne->model() );    //Для первого предмета
    QStandardItem* item = model->item(priveousSubjectThree);
    item->setEnabled( true );
    item = model->item( index );
    item->setEnabled( false );

    model = dynamic_cast< QStandardItemModel * >( ui->subjectTwo->model() );    //Для второго предмета
    item = model->item(priveousSubjectThree);
    item->setEnabled( true );
    item = model->item( index );
    item->setEnabled( false );

    model = dynamic_cast< QStandardItemModel * >( ui->exam_sub->model() );  //Для модификации
    item = model->item(priveousSubjectThree);
    item->setEnabled( false );
    item = model->item( index );
    item->setEnabled( true );

    //Выбор экзамена в зависимости от предметов
    if (ui->exam_sub->currentIndex() != ui->subjectOne->currentIndex() &&
            ui->exam_sub->currentIndex() != ui->subjectTwo->currentIndex())
    {
        ui->exam_sub->setCurrentIndex(index);
    }
    priveousSubjectThree = index;
}

//Активирование/диактивирование виджетов
void MainWindow::doEnabled(bool arg)
{
    ui->FIO->setEnabled(arg);
    ui->DateOfBirth->setEnabled(arg);
    ui->gender->setEnabled(arg);
    ui->presenceOfMedal->setEnabled(arg);
    ui->averageRating->setEnabled(arg);
    ui->chooseLanguages->setEnabled(arg);
    ui->subjectOne->setEnabled(arg);
    ui->subjectTwo->setEnabled(arg);
    ui->subjectThree->setEnabled(arg);
    ui->scoreOne->setEnabled(arg);
    ui->scoreTwo->setEnabled(arg);
    ui->scoreThree->setEnabled(arg);
    ui->contractualStudy->setEnabled(arg);
    ui->exam_sub->setEnabled(arg);
    ui->credit->setEnabled(arg);
    ui->deleteButton->setEnabled(arg);
}

//Активировать/Диактивировать поля новой записи, все кроме ФИО
void MainWindow::doEnabledNewRecord(bool arg)
{
    ui->gender->setEnabled(arg);
    ui->presenceOfMedal->setEnabled(arg);
    ui->averageRating->setEnabled(arg);
    ui->chooseLanguages->setEnabled(arg);
    ui->subjectOne->setEnabled(arg);
    ui->subjectTwo->setEnabled(arg);
    ui->subjectThree->setEnabled(arg);
    ui->scoreOne->setEnabled(arg);
    ui->scoreTwo->setEnabled(arg);
    ui->scoreThree->setEnabled(arg);
    ui->contractualStudy->setEnabled(arg);
    ui->exam_sub->setEnabled(arg);
    ui->credit->setEnabled(arg);
}

//Начальные параметры главного окна
void MainWindow::start()
{
    //Валидатор
    QValidator *ev = new Validator(this);
    ui->FIO->setValidator(ev);

    //Ограничения по дате рождения
    ui->DateOfBirth->setMinimumDate(QDate::currentDate().addDays(-18250));
    ui->DateOfBirth->setMaximumDate(QDate::currentDate().addDays(-5475));

    //Установка начальных предметов по умолчанию
    ui->subjectTwo->setCurrentIndex(1);
    ui->subjectThree->setCurrentIndex(2);

    //Установка начальных ограничение по возможности выбора предмета
    QStandardItemModel* subTwo = dynamic_cast< QStandardItemModel * >( ui->subjectTwo->model() );    //Запоминаем QComboBox
    QStandardItem* item = subTwo->item( 0 ); //Выбираем ячейку в выбранном виджете
    item->setEnabled( false );  //Блокируем выбранную ячейку

    QStandardItemModel* subThree = dynamic_cast< QStandardItemModel * >( ui->subjectThree->model() );
    item = subThree->item( 0 );
    item->setEnabled( false );

    QStandardItemModel* subOne = dynamic_cast< QStandardItemModel * >( ui->subjectOne->model() );
    item = subOne->item( 1 );
    item->setEnabled( false );

    subThree = dynamic_cast< QStandardItemModel * >( ui->subjectThree->model() );
    item = subThree->item( 1 );
    item->setEnabled( false );

    subOne = dynamic_cast< QStandardItemModel * >( ui->subjectOne->model() );
    item = subOne->item( 2 );
    item->setEnabled( false );

    subTwo = dynamic_cast< QStandardItemModel * >( ui->subjectTwo->model() );
    item = subTwo->item( 2 );
    item->setEnabled( false );

    //Для модификации
    QStandardItemModel* exam1 = dynamic_cast< QStandardItemModel * >( ui->exam_sub->model() );
    item = exam1->item( 3 );
    item->setEnabled( false );

    exam1 = dynamic_cast< QStandardItemModel * >( ui->exam_sub->model() );
    item = exam1->item( 4 );
    item->setEnabled( false );

    doEnabled(false);   //Заблокировать все поля кроме кнопок заполнить и создать
    arguments << "count";
    launchServ(arguments);
    QString str = getResultFromServer();
    QStringList copyStr = str.split('!');
    int count = copyStr[0].toInt();
    lastId = copyStr[1].toInt();
    for (int i = 0; i < count; i++)
    {
        arguments.clear();
        arguments << "getRecord" << QString::number(i);
        launchServ(arguments);
        str = getResultFromServer();
        curApp = curApp.fromServer(str);
        addRecord(curApp);
    }
}

void MainWindow::launchServ(QStringList args)
{
    server = new QProcess(this);
    server->setProgram("D:\\c#_projects\\ОС_C#_1\\server\\server\\bin\\Debug\\server.exe");
    server->setArguments(args);
    server->start();
    qDebug() << server->state();
    server->waitForFinished(-1);
    Sleep(125);
    delete server;
}

QString MainWindow::getResultFromServer()
{
    LPCSTR subkey = "Environment";
    HKEY key;
    RegOpenKeyExA(HKEY_CURRENT_USER, subkey, 0, KEY_QUERY_VALUE, &key);
    WCHAR value[1024] = {};
    DWORD dwvalue = sizeof(value);
    DWORD type = REG_SZ;
    RegQueryValueExW(key, L"myenv", NULL, &type, (LPBYTE)&value, &dwvalue);
    int len = (dwvalue / sizeof(WCHAR));
    if ((len > 0) && (value[len-1] == 0))
    --len;
    char strval[1024];
    WideCharToMultiByte(CP_UTF8, 0, value, len + 40, strval, len + 40, NULL, NULL);
    strval[len + 40] = '\0';

    RegCloseKey(key);
    return QString(strval);
}


//Добавление заявления в таблицу
void MainWindow::addAppToTable(const Application &app)
{
    int newPos = ui->recBrowser->rowCount();    //Добавить новую запись в конец таблицы

    //Добавить запись в таблицу
    ui->recBrowser->insertRow(newPos);  //Добавить строку
    QTableWidgetItem* newItem;
    newItem = new QTableWidgetItem("ФИО");  //Добавить ФИО
    newItem->setData(Qt::UserRole, app.getId());    //Вшить айдишник в ФИО
    ui->recBrowser->setItem(newPos, 0, newItem);

    newItem = new QTableWidgetItem("01.01.2000");//Добавить дату Рождения
    ui->recBrowser->setItem(newPos, 1, newItem);

    newItem = new QTableWidgetItem("0");    //Добавить средний балл
    ui->recBrowser->setItem(newPos, 2, newItem);

    ui->recBrowser->setCurrentCell(newPos, 0);  //Выделить запись
    currentRec=app.getId(); //Обновить номер текущей записи
}

//Показать текущую запись
void MainWindow::showRecordData(const Application &currentApp)
{
    perehod = 1;
    ui->FIO->setText(currentApp.getName());
    ui->DateOfBirth->setDate(currentApp.getDateBirth());
    ui->gender->setCurrentIndex(currentApp.getIndexGender());
    ui->presenceOfMedal->setChecked(currentApp.getHaveAMedal());
    ui->averageRating->setValue(currentApp.getAverageValue());
    ui->chooseLanguages->setCurrentIndex(currentApp.getLanguage());
    ui->subjectOne->setCurrentIndex(currentApp.getSubject(0));
    ui->subjectTwo->setCurrentIndex(currentApp.getSubject(1));
    ui->subjectThree->setCurrentIndex(currentApp.getSubject(2));
    ui->scoreOne->setValue(currentApp.getRaitingOne());
    ui->scoreTwo->setValue(currentApp.getRaitingTwo());
    ui->scoreThree->setValue(currentApp.getRaitingThree());
    ui->contractualStudy->setChecked(currentApp.getPayStudy());
    ui->credit->setChecked(currentApp.getExam());
    ui->exam_sub->setCurrentIndex(currentApp.getIndexExam());
    perehod = 0;
}

//Изменить данные в таблице
void MainWindow::changeRecInList(const Application &app)
{
    for (int i = 0; i < ui->recBrowser->rowCount(); i++)    //Найти нужную запись
    {
        if (app.getId() == ui->recBrowser->item(i, 0)->data(Qt::UserRole).toInt())  //Если запись найдена
        {
            //Изменить элементы записи в таблице
            ui->recBrowser->item(i, 0)->setText(ui->FIO->text());
            ui->recBrowser->item(i, 1)->setText(ui->DateOfBirth->date().toString("dd.MM.yyyy"));
            double a = floor((ui->scoreOne->value() + ui->scoreTwo->value() + ui->scoreThree->value()) * 10 / 3) / 10;
            QString toStr = QString::number(a);
            ui->recBrowser->item(i, 2)->setText(toStr);
            break;
        }
    }
}

//Добавить готовую запись
void MainWindow::addRecord(Application &rec)
{
    addAppToTable(rec); //Создать запись в таблице
    changeRecInList(rec);   //Изменить элементы записи в таблице
}

//Создание нового заявления
void MainWindow::on_createButton_clicked()
{
    Application newApp; //Создание объекта класса
    newApp.createDefaultRec();  //Занести в объект стандартные значения
    lastId++;
    QString str = newApp.recordInStr(lastId);
    newApp.setId(lastId);
    curApp = newApp;
    arguments.clear();
    arguments << "new record" << str;
    launchServ(arguments);
    /*QString code = "new record";
    socket.write(QString(code + "\n").toUtf8());
    socket.waitForReadyRead(125);
    socket.write(QString(str + "\n").toUtf8());
    socket.waitForReadyRead(125);*/
    addAppToTable(curApp);  //Создать запись в таблице
}

//Откытие содержимого при изменении фокуса в таблице
void MainWindow::on_recBrowser_currentItemChanged(QTableWidgetItem *current, QTableWidgetItem *previous)
{
    if (current != nullptr && current != previous && stop == 0)  //Если текущий элемент не равен предыдущему и не равен пустому месту
    {
        //socket.write(QString(QString("changed focus") + "\n").toUtf8());
        //socket.waitForReadyRead(125);
        arguments.clear();

        int currentId = ui->recBrowser->item(current->row(), 0)->data(Qt::UserRole).toInt();
        arguments << "changed focus" <<  QString::number(currentId) << QString::number(ui->recBrowser->currentRow());                                           //Получить ID текущей записи
        launchServ(arguments);
        QString str = getResultFromServer();
        //socket.write(QString(QString::number(currentId) + "\n").toUtf8());
        //socket.waitForReadyRead(125);

        //socket.waitForReadyRead(125);
        //QString str = socket.readLine(10000);
        curApp = curApp.fromServer(str);   //Получить текущую запись
        currentRec = currentId; //Обновить текущую запись
        //socket.write(QString(QString::number(current->row()) + "\n").toUtf8());
        //socket.waitForReadyRead(125);
        ui->recBrowser->setCurrentCell(current->row(), 0);  //Выделить текущую запись
        perehod = 1;    //Переход активирован

        //Вывод значений записи на экран
        ui->FIO->setText(curApp.getName());
        ui->DateOfBirth->setDate(curApp.getDateBirth());
        ui->gender->setCurrentIndex(curApp.getIndexGender());
        ui->presenceOfMedal->setChecked(curApp.getHaveAMedal());
        ui->averageRating->setValue(curApp.getAverageValue());
        ui->chooseLanguages->setCurrentIndex(curApp.getLanguage());
        ui->subjectOne->setCurrentIndex(curApp.getSubject(0));
        ui->subjectTwo->setCurrentIndex(curApp.getSubject(1));
        ui->subjectThree->setCurrentIndex(curApp.getSubject(2));
        ui->scoreOne->setValue(curApp.getRaitingOne());
        ui->scoreTwo->setValue(curApp.getRaitingTwo());
        ui->scoreThree->setValue(curApp.getRaitingThree());
        ui->contractualStudy->setChecked(curApp.getPayStudy());
        ui->credit->setChecked(curApp.getExam());
        ui->exam_sub->setCurrentIndex(curApp.getIndexExam());
        perehod = 0;    //Переход завершен
    }
    else if(current == nullptr) //Если текущий элемент нулевой адрес
    {
        currentRec = -1;    //Текущей аписи нет
    }
    if (ui->recBrowser->rowCount() == 0)    //Если записей больше нет
    {
    doEnabled(false);   //Диактивировать поля
    }
    else    //Иначе
    {
        doEnabled(true);    //Активировать поля
    }
}


//Изменение даты дня рождения
void MainWindow::on_DateOfBirth_userDateChanged(const QDate &date)
{
    if (empty == 0)
    {
    ui->recBrowser->item(ui->recBrowser->currentRow(), 1)->setText(ui->DateOfBirth->date().toString("dd.MM.yyyy")); //Изменить дату в таблице
    }
}

//Обновление записи в браузере при изменении
void MainWindow::updateRecInBrowser()
{
    if (currentRec != -1 && perehod != 1 && ui->recBrowser->rowCount() != 1)    //Если текущая запись существует и нет перехода и количество записей не равно единице
    {
        changeRecInList(curApp);   //Изменить элементы в записи
    }
    if (ui->recBrowser->rowCount() == 1)    //Если количество строк в таблице равно единице
    {
        changeRecInList(curApp);   //Изменить элементы в записи
    }
}

//Удалить запись
void MainWindow::on_deleteButton_clicked()
{
    //Контейнеры для элементов
    QTableWidgetItem* newItem0;
    QTableWidgetItem* newItem1;
    QTableWidgetItem* newItem2;
    int removeRow;  //Строка которую удаляем

    arguments.clear();
    arguments << "delete record" << QString::number(currentRec);
    launchServ(arguments);
    //QString code = "delete record";
    //socket.write(QString(code + "\n").toUtf8());
    //socket.waitForReadyRead(125);
    QString str = QString::number(currentRec);
    //socket.write(QString(str + "\n").toUtf8());
    //socket.waitForReadyRead(125);

            //Запоминаем номер строки, удаляем строку в браузере записей
    removeRow = ui->recBrowser->currentRow();
    newItem0 = ui->recBrowser->takeItem(ui->recBrowser->currentRow(), 0);
    newItem1 = ui->recBrowser->takeItem(ui->recBrowser->currentRow(), 1);
    newItem2 = ui->recBrowser->takeItem(ui->recBrowser->currentRow(), 2);
    ui->recBrowser->removeRow(ui->recBrowser->currentRow());

    if (ui->recBrowser->rowCount() == 0)    //Если после удаления записей не осталось -> начальные параметры
    {
        empty = 1;
        Application app = Application();
        showRecordData(app);
        doEnabled(false);
        currentRec = -1;
        empty = 0;
    }
    else if ((ui->recBrowser->rowCount() - removeRow) == 0) //Если элемент находился в самом низу списка
    {
        ui->recBrowser->setCurrentCell(removeRow - 1, 0);   //Выделить последнюю строку праузера
        str = QString::number(removeRow - 1);
    }
    else if((ui->recBrowser->rowCount() - removeRow) > 0)   //Если элеемент находился где-то в середине или начале
    {
        ui->recBrowser->setCurrentCell(removeRow, 0);   //Выделить следующую запись
        str = QString::number(removeRow);
    }
}

void MainWindow::on_FIO_editingFinished()
{
    if (empty == 0 && perehod == 0 && curApp.getName() != ui->FIO->text())
    {
    if (ui->FIO->hasAcceptableInput()) //Если ФИО корректно
    {
        doEnabledNewRecord(true);   //Активировать недоступные поля
        //QString code = "changed FIO";
        //socket.write(QString(code + "\n").toUtf8());
        //socket.waitForReadyRead(125);
        //socket.waitForReadyRead(125);
        //code = socket.readLine(10000);
        arguments.clear();
        arguments << "changed FIO" << QString::number(currentRec);
        launchServ(arguments);
        QString code = getResultFromServer();
        curApp = curApp.fromServer(code);
        curApp.setName(ui->FIO->text());
        code = curApp.recordInStr(curApp.getId());
        arguments.clear();
        arguments << "changedById" << code << QString::number(currentRec);
        launchServ(arguments);
        //socket.write(QString(code + "\n").toUtf8());
        //socket.waitForReadyRead(125);   //Сохранить текущее значение ФИО
        changeRecInList(curApp);
    }
    else if (ui->FIO->text() == "ФИО") //Если Текущая запись "ФИО"
    {
        doEnabledNewRecord(false);  //Диактивировать все поля кроме ФИО и кнопок
    }
    }
}

void MainWindow::on_scoreOne_editingFinished()
{
    if (empty == 0 && perehod == 0 && curApp.getRaitingOne() != ui->scoreOne->value())
    {
    /*QString code = "changed scoreOne";
    socket.write(QString(code + "\n").toUtf8());
    socket.waitForReadyRead(125);
    socket.waitForReadyRead(125);
    code = socket.readLine(10000);*/
        arguments.clear();
        arguments << "changed scoreOne" << QString::number(currentRec);
        launchServ(arguments);
        QString code = getResultFromServer();
    curApp = curApp.fromServer(code);

    curApp.setRaitingOne(ui->scoreOne->value());   //Если баллы по первому предмету 90 и выше
    code = curApp.recordInStr(curApp.getId());
    arguments.clear();
    arguments << "changedById" << code << QString::number(currentRec);
    launchServ(arguments);
    /*socket.write(QString(code + "\n").toUtf8());
    socket.waitForReadyRead(125);*/
    changeRecInList(curApp);
    }
}

void MainWindow::on_scoreTwo_editingFinished()
{
    if (empty == 0 && perehod == 0 && curApp.getRaitingTwo() != ui->scoreTwo->value())
    {
        /*QString code = "changed scoreTwo";
        socket.write(QString(code + "\n").toUtf8());
        socket.waitForReadyRead(125);
        //Application app;
        socket.waitForReadyRead(125);
        code = socket.readLine(10000);*/
        arguments.clear();
        arguments << "changed scoreTwo" << QString::number(currentRec);
        launchServ(arguments);
        QString code = getResultFromServer();
        curApp = curApp.fromServer(code);
        curApp.setRaitingTwo(ui->scoreTwo->value());
        code = curApp.recordInStr(curApp.getId());
        arguments.clear();
        arguments << "changedById" << code << QString::number(currentRec);
        launchServ(arguments);
        /*socket.write(QString(code + "\n").toUtf8());
        socket.waitForReadyRead(125);*/ //Сохранить текущее значение
        changeRecInList(curApp);
    }
}

void MainWindow::on_scoreThree_editingFinished()
{
    if (empty == 0 && perehod == 0 && curApp.getRaitingThree() != ui->scoreThree->value())
    {
        /*QString code = "changed scoreThree";
        socket.write(QString(code + "\n").toUtf8());
        socket.waitForReadyRead(125);
        socket.waitForReadyRead(125);
        code = socket.readLine(10000);*/
        arguments.clear();
        arguments << "changed scoreThree" << QString::number(currentRec);
        launchServ(arguments);
        QString code = getResultFromServer();
        curApp = curApp.fromServer(code);
        curApp.setRaitingThree(ui->scoreThree->value());
        code = curApp.recordInStr(curApp.getId());
        arguments.clear();
        arguments << "changedById" << code << QString::number(currentRec);
        launchServ(arguments);
        /*socket.write(QString(code + "\n").toUtf8());
        socket.waitForReadyRead(125);*/  //Сохранить текщее значение
        changeRecInList(curApp);
    }
}
