#ifndef MAINWINDOW_H
#define MAINWINDOW_H
//#include "appdatabase.h"
#include <QMainWindow>
#include "validator.h"
#include <QDate>
#include "Application.h"
#include "QTableWidgetItem"
#include <QLocalServer>
#include <QLocalSocket>
#include <QDataStream>
#include <QProcess>
#include <QTimer>
#include <windows.h>
#include <QProcess>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QLocalSocket socket;
    QProcess* server;
private slots:
    void saveData();    //Хранение значений

    void start();   //Начальные данные для приложения

    void doEnabled(bool arg);   //Активация/диактивация виджетов

    void doEnabledNewRecord(bool arg);  //Активация/диактивация новой хзаписи

    //void on_presenceOfMedal_stateChanged(int arg1); //Ограничение на средний балл

    //void on_scoreOne_valueChanged(int arg1);    //Блокировка/разблокировка виджетов при выборе баллов первого предмета

    void on_subjectOne_currentIndexChanged(int index);  //Выбрать первый предмет

    void on_subjectTwo_currentIndexChanged(int index);  //Выбрать второй предмет

    void on_subjectThree_currentIndexChanged(int index);    //Выбрать третий предмет

    void on_createButton_clicked(); //Создать запись

    void on_recBrowser_currentItemChanged(QTableWidgetItem *current, QTableWidgetItem *previous);   //переход к другой записи

    //void on_FIO_textChanged(const QString &arg1);   //Изменение ФИО

    //void on_scoreTwo_valueChanged(int arg1);    //Изменение баллов за второй предмет

    //void on_scoreThree_valueChanged(int arg1);  //Изменение баллов за третий предмет

    void on_DateOfBirth_userDateChanged(const QDate &date); //Изменение джаты рождения

    void on_deleteButton_clicked(); //Удалить запись

    //void on_fillButton_clicked();   //Заполнить таблицу готовыми записями

    //void on_deductButton_clicked();

    void on_FIO_editingFinished();

    void on_scoreOne_editingFinished();

    void on_scoreTwo_editingFinished();

    void on_scoreThree_editingFinished();

    //void on_FIO_textChanged(const QString &arg1);

private:
    QStringList arguments;
    void launchServ(QStringList args);
    QString getResultFromServer();
    void addAppToTable(const Application& app); //Добавить запись в таблицу
    int findNewPosForInsert(const Application& app);    //Найти позицию для вставки
    void showRecordData(const Application& currentApp); //Показать данные на виджетах
    void changeRecInList(const Application& app);   //Изменить элементы в записи
    void changePosForTable(const int ID, const int NewID);  //Изменить позицию в таблице
    void addRecord(Application& rec);   //Добавить созданную запись
    void updateRecInBrowser();  //Обновить положение записи в таблице
    int perehod = 0;    //Флаг перехода
    Ui::MainWindow *ui;
    //appDataBase currentSave;    //Объект контейнера записей
    Application* item;
    Application curApp = Application();
    int changed = true;
    int lastId = 0;
    int stop = 0;
    int currentRec = -1;    //Текущая запись
    bool onlyChanged = false;
    int empty = 0;  //Срабатывает при удалении последней записи в браузере
    int priveousSubjectOne = 0; //Предыдущий первый предмет
    int priveousSubjectTwo = 1; //Предыдущий второй предмет
    int priveousSubjectThree = 2;   //Предыдущий третий предмет
    bool stopAll = true;
};

#endif // MAINWINDOW_H
