#include "validator.h"
#include "qstring.h"

Validator::Validator(QObject* parent)
    : QValidator (parent)
{

}
Validator::~Validator()
{

}

QValidator::State Validator::validate(QString &string, int &pos) const
{
    QRegExp acceptText("^[А-Я][а-я]+(\\-[А-Я][а-я]+)?\\s[А-Я][а-я]+\\s[А-Я][а-я]+$");   //Правильная форма строки
    QRegExp interText("^[А-Я]?[а-я]*(\\-?[А-Я]?[а-я]*)?\\s?[А-Я]?[а-я]*\\s?[А-Я]?[а-я]*$");    //Форма строки для заполнения

    if (acceptText.exactMatch(string)) return QValidator::Acceptable;   //Если строка полностью введена верно
    if (interText.exactMatch(string)) return QValidator::Intermediate;  //Если строка частично введена верно

    return QValidator::Invalid; //Если строка введена неверно
}































//Выбор пола
//ui->gender->addItem("Муж");
//ui->gender->addItem("Жен");

//Выбор иностранного языка
//ui->ForeignLanguages->setSelectionMode(QAbstractItemView::MultiSelection);  //Запоминание нескольких значений
/*ui->ForeignLanguages->item(0)->setSelected(1);
ui->ForeignLanguages->item(5)->setSelected(1);*/

//Первая запись
/*void MainWindow::on_pushButtonOne_clicked()
{
    if (!ui->FIO->hasAcceptableInput()) //Срабатывает если ФИО введено неверно и возвращает фокус на строку
    {
        QMessageBox::warning(this, "ФИО некорректно", "Введите пожалуйста ФИО правильно");
    }
    else //Выводит значения хранящиеся в записи 2
    {
    ui->labelRecord->setText("Запись 1");
    currentSave = 0;
    ui->FIO->setText(save[currentSave].getName());
    ui->DateOfBirth->setDate(save[currentSave].getDateBirth());
    ui->gender->setCurrentIndex(save[currentSave].getIndexGender());
    ui->presenceOfMedal->setChecked(save[currentSave].getHaveAMedal());
    ui->averageRating->setValue(save[currentSave].getAverageValue());
    ui->chooseLanguages->setCurrentIndex(save[currentSave].getLanguage());
    ui->subjectOne->setCurrentIndex(save[currentSave].getSubject(0));
    ui->subjectTwo->setCurrentIndex(save[currentSave].getSubject(1));
    ui->subjectThree->setCurrentIndex(save[currentSave].getSubject(2));
    ui->scoreOne->setValue(save[currentSave].getRaitingOne());
    ui->scoreTwo->setValue(save[currentSave].getRaitingTwo());
    ui->scoreThree->setValue(save[currentSave].getRaitingThree());
    ui->contractualStudy->setChecked(save[currentSave].getPayStudy());
    ui->credit->setChecked(save[currentSave].getExam());
    ui->exam_sub->setCurrentIndex(save[currentSave].getIndexExam());
    on_subjectOne_currentIndexChanged(ui->subjectOne->currentIndex());  //Принудительный вызов для обновления списка экзамена
    on_subjectTwo_currentIndexChanged(ui->subjectTwo->currentIndex());
    on_subjectThree_currentIndexChanged(ui->subjectThree->currentIndex());
    }
}*/

//Вторая запись
/*void MainWindow::on_pushButtonTwo_clicked()
{
    if (!ui->FIO->hasAcceptableInput()) //Срабатывает если ФИО введено неверно и возвращает фокус на строку
    {
        QMessageBox::warning(this, "ФИО некорректно", "Введите пожалуйста ФИО правильно");
    }
    else //Выводит значения хранящиеся в записи 2
    {
    ui->labelRecord->setText("Запись 2");
    currentSave = 1;
    ui->FIO->setText(save[currentSave].getName());
    ui->DateOfBirth->setDate(save[currentSave].getDateBirth());
    ui->gender->setCurrentIndex(save[currentSave].getIndexGender());
    ui->presenceOfMedal->setChecked(save[currentSave].getHaveAMedal());
    ui->averageRating->setValue(save[currentSave].getAverageValue());
    ui->chooseLanguages->setCurrentIndex(save[currentSave].getLanguage());
    ui->subjectOne->setCurrentIndex(save[currentSave].getSubject(0));
    ui->subjectTwo->setCurrentIndex(save[currentSave].getSubject(1));
    ui->subjectThree->setCurrentIndex(save[currentSave].getSubject(2));
    ui->scoreOne->setValue(save[currentSave].getRaitingOne());
    ui->scoreTwo->setValue(save[currentSave].getRaitingTwo());
    ui->scoreThree->setValue(save[currentSave].getRaitingThree());
    ui->contractualStudy->setChecked(save[currentSave].getPayStudy());
    ui->credit->setChecked(save[currentSave].getExam());
    ui->exam_sub->setCurrentIndex(save[currentSave].getIndexExam());
    on_subjectOne_currentIndexChanged(ui->subjectOne->currentIndex());  //Принудительный вызов для обновления списка экзамена
    on_subjectTwo_currentIndexChanged(ui->subjectTwo->currentIndex());
    on_subjectThree_currentIndexChanged(ui->subjectThree->currentIndex());
    }
}*/

//void on_pushButtonOne_clicked();    //Первая запись

//void on_pushButtonTwo_clicked();    //Вторая запись



/*Application newApp;
newApp.setName(ui->FIO->text());
newApp.setDateBirth(ui->DateOfBirth->date());
newApp.setIndexGender(ui->gender->currentIndex());
newApp.setHaveAMedal(ui->presenceOfMedal->isChecked());
newApp.setAverageValue(ui->averageRating->value());
newApp.setLanguage(ui->chooseLanguages->currentIndex());
newApp.setSubject(0, ui->subjectOne->currentIndex());
newApp.setSubject(1, ui->subjectTwo->currentIndex());
newApp.setSubject(2, ui->subjectThree->currentIndex());
newApp.setRaitingOne(ui->scoreOne->value());
newApp.setRaitingTwo(ui->scoreTwo->value());
newApp.setRaitingThree(ui->scoreThree->value());
newApp.setIndexExam(ui->exam_sub->currentIndex());
newApp.setExam(ui->credit->isChecked());
newApp.setPayStudy(ui->contractualStudy->isChecked());
currentSave.createApp(newApp);
addAppToTable(newApp);*/

/*int newPos = findNewPosForInsert(app);
ui->recBrowser->insertRow(newPos);
QTableWidgetItem* item;
item = new QTableWidgetItem(ui->FIO->text());
item->setData(Qt::UserRole, app.getId());
ui->recBrowser->setItem(newPos, 0, item);

item = new QTableWidgetItem(ui->DateOfBirth->date().toString("dd.MM.yyyy"));
ui->recBrowser->setItem(newPos, 1, item);

double a = floor((ui->scoreOne->value() + ui->scoreTwo->value() + ui->scoreThree->value()) * 10 / 3) / 10;
QString toStr = QString::number(a);
item = new QTableWidgetItem(toStr);
ui->recBrowser->setItem(newPos, 2, item);

ui->recBrowser->setCurrentCell(newPos, 0);*/






/*if (currentRec != -1 && perehod != 1 && ui->recBrowser->rowCount() != 1)
{
    int previousPos = ui->recBrowser->currentRow();
    int newPos = findNewPosForInsert(currentSave.getbyId(currentRec));
    if (previousPos != newPos)
    {
        changePosForTable(previousPos, newPos);
    }
    ui->recBrowser->setCurrentCell(newPos, 0);
    changeRecInList(currentSave.getbyId(currentRec));
}
if (ui->recBrowser->rowCount() == 1)
{
    changeRecInList(currentSave.getbyId(currentRec));
}*/
/*if (currentRec != -1 && perehod != 1 && ui->recBrowser->rowCount() != 1)
{
    int previousPos = ui->recBrowser->currentRow();
    int newPos = findNewPosForInsert(currentSave.getbyId(currentRec));
    if (previousPos != newPos)
    {
        changePosForTable(previousPos, newPos);
    }
    ui->recBrowser->setCurrentCell(newPos, 0);
    changeRecInList(currentSave.getbyId(currentRec));
}
if (ui->recBrowser->rowCount() == 1)
{
    changeRecInList(currentSave.getbyId(currentRec));
}*/
/*if (currentRec != -1 && perehod != 1 && ui->recBrowser->rowCount() != 1)
{
    int previousPos = ui->recBrowser->currentRow();
    int newPos = findNewPosForInsert(currentSave.getbyId(currentRec));
    if (previousPos != newPos)
    {
        changePosForTable(previousPos, newPos);
    }
    ui->recBrowser->setCurrentCell(newPos, 0);
    changeRecInList(currentSave.getbyId(currentRec));
}
if (ui->recBrowser->rowCount() == 1)
{
    changeRecInList(currentSave.getbyId(currentRec));
}*/
/*if (currentRec != -1 && perehod != 1 && ui->recBrowser->rowCount() != 1)
{
    int previousPos = ui->recBrowser->currentRow();
    int newPos = findNewPosForInsert(currentSave.getbyId(currentRec));
    if (previousPos != newPos)
    {
        changePosForTable(previousPos, newPos);
    }
    ui->recBrowser->setCurrentCell(newPos, 0);
    changeRecInList(currentSave.getbyId(currentRec));
}
if (ui->recBrowser->rowCount() == 1)
{
    changeRecInList(currentSave.getbyId(currentRec));
}*/
